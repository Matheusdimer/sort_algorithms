package com.dimer.algoritmos.outros;

import java.util.ArrayList;
import java.util.List;

public class Troco {

    public static List<Integer> guloso(int[] moedas, int valorTroco) {
        final List<Integer> moedasTroco = new ArrayList<>();

        for (int valor : moedas) {
            while (valor <= valorTroco) {
                moedasTroco.add(valor);
                valorTroco = valorTroco - valor;
            }
        }

        return moedasTroco;
    }
}
