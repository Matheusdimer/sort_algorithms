package com.dimer.algoritmos.outros;

public class Pesquisa {
    public static int sequencial(int[] array, int search) {
        for (int i = 0; i < array.length; i++) {
            if (search == array[i]) {
                return i;
            }
        }
        return -1;
    }

    public static int binaria(int[] array, int search) {
        return binaria(array, search, 0, array.length - 1);
    }

    private static int binaria(int[] array, int search, int left, int right) {
        int middle = (right + left) / 2;
        int middleItem = array[middle];

        if (middleItem == search) {
            return middle;
        }

        if (middleItem < search) {
            return binaria(array, search, middle, right);
        } else {
            return binaria(array, search, left, middle);
        }
    }
}
