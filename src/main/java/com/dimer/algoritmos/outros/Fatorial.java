package com.dimer.algoritmos.outros;

public class Fatorial {

    public static int recursivo(final int numero) {
        return recursivo(numero, numero - 1);
    }

    private static int recursivo(int numero, int i) {
        if (i > 0) {
            numero *= i;
            i--;
            return recursivo(numero, i);
        }
        return numero;
    }

    public static int iterativo(final int numero) {
        int resultado = numero;

        for (int i = numero - 1; i > 0; i--) {
            resultado *= i;
        }

        return resultado;
    }

}
