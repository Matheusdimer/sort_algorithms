package com.dimer.algoritmos.outros;

import java.util.Comparator;

public class CombSort {

    private int getNextGap(int gap) {
        gap = (gap*10)/13;
        if (gap < 1)
            return 1;
        return gap;
    }

    public <T> void sort(T[] arr, Comparator<T> comparator) {
        int n = arr.length;
        int gap = n;

        boolean swapped = true;

        while (gap != 1 || swapped == true) {
            gap = getNextGap(gap);
            swapped = false;

            for (int i=0; i<n-gap; i++)
            {
                if (comparator.compare(arr[i], arr[i+gap]) > 0) {
                    T temp = arr[i];
                    arr[i] = arr[i+gap];
                    arr[i+gap] = temp;

                    swapped = true;
                }
            }
        }
    }
}
