package com.dimer.algoritmos.outros;

import java.util.Comparator;

public class SelectionSort {

    public <T> void sort(T[] array, Integer length, Comparator<T> comparator) {
        for (int i = 0; i < length - 1; i++)
        {
            int index = i;
            for (int j = i + 1; j < length; j++){
                if (comparator.compare(array[j], array[index]) < 0){
                    index = j;
                }
            }
            T smallerNumber = array[index];
            array[index] = array[i];
            array[i] = smallerNumber;
        }
    }
}
