package com.dimer.algoritmos.outros;

public class Fibonacci {

    public static int iterativo(int n) {
        int anterior = 0, soma = 1, aux;

        for (int i = 0; i < n - 1; i++) {
            aux = soma;
            soma += anterior;
            anterior = aux;
        }

        return soma;
    }

    public static int recursivo(int n) {
        if (n == 1) {
            return 1;
        } else if (n == 2) {
            return 1;
        } else {
            return recursivo(n - 1) + recursivo(n - 2);
        }
    }
}
