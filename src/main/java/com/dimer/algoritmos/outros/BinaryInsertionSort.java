package com.dimer.algoritmos.outros;

/*
Esta ordenaçção só ordena (INT) não funciona com generics
* */

public class BinaryInsertionSort {

    public void sort(Integer[] arr, int n) {
        int i, loc, j, k, selected;

        for (i = 1; i < n; ++i) {
            j = i - 1;
            selected = arr[i];

            loc = binary(arr, selected, 0, j);

            while (j >= loc) {
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = selected;
        }
    }

    public int binary(Integer[] a,int item, int low, int high) {
        while (low <= high) {
            int mid = low + (high - low) / 2;
            if (item == a[mid])
                return mid + 1;
            else if (item > a[mid])
                low = mid + 1;
            else
                high = mid - 1;
        }

        return low;
    }
}
