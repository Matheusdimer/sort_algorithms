package com.dimer.algoritmos;

import com.dimer.algoritmos.ordenacao.*;

import java.util.Comparator;

import static com.dimer.algoritmos.ordenacao.SortUtils.*;

/*
* @Authors: Matheus de Lima Dimer & Raí Bonassa Martins
* @Description: Este programa foi desenvolvido para usar e estudar sobre os algorítimos de ordenação.
* Estão disponíveis: BubbleSort, InsertionSort,ShellSort, QuickSort e MergeSort
* */
public class OrdenacaoNumeros {
    // Número de vezes em que os algoritmos serão executados
    private static final int N = 5;

    class Pessoa {
        private int id;
        private String nome;
    }

    public static void main(String[] args) {
        Excel excel = new Excel();
        BubbleSort bubbleSort = new BubbleSort();
        InsertionSort insertionSort = new InsertionSort();
        ShellSort shellSort = new ShellSort();
        QuickSort quickSort = new QuickSort();
        MergeSort mergeSort = new MergeSort();

        ExecutionLog executionLog;

        createIntegerArray(100);

        Integer[] array = getIntegerArray();

        /* Execução de aquecimento */
        quickSort.sort(array, Comparator.comparingInt(a -> a));

        for (int i = 0; i < N; i++) {
            System.out.println("\nExecutando bubble sort...");

            array = getIntegerArray();

            start();
            executionLog = bubbleSort.sort(array, (a, b) -> a - b);

            executionLog.setTime(end());
            executionLog.print();

            excel.writeLogRow(executionLog);
        }

        for (int i = 0; i < N; i++) {
            System.out.println("\nExecutando insertion sort...");

            array = getIntegerArray();

            start();
            executionLog = insertionSort.sort(array, Comparator.comparingInt(a -> a));

            executionLog.setTime(end());
            executionLog.print();

            excel.writeLogRow(executionLog);
        }

        for (int i = 0; i < N; i++) {
            System.out.println("\nExecutando shell sort...");

            array = getIntegerArray();

            start();

            executionLog = shellSort.sort(array, Comparator.comparingInt(a -> a));

            executionLog.setTime(end());
            executionLog.print();

            excel.writeLogRow(executionLog);
        }

        for (int i = 0; i < N; i++) {
            System.out.println("\nExecutando quick sort...");

            array = getIntegerArray();

            start();
            executionLog = quickSort.sort(array, Comparator.comparingInt(a -> a));

            executionLog.setTime(end());
            executionLog.print();

            excel.writeLogRow(executionLog);
        }

        for (int i = 0; i < N; i++) {
            System.out.println("\nExecutando merge sort...");

            array = getIntegerArray();

            start();

            executionLog = mergeSort.sort(array, 0,array.length-1, Comparator.comparingInt(a -> a));

            executionLog.setTime(end());
            executionLog.print();

            excel.writeLogRow(executionLog);
        }

        excel.save();
    }
}
