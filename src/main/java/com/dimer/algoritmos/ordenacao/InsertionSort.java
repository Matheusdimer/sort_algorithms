package com.dimer.algoritmos.ordenacao;

import java.util.Comparator;
import java.util.concurrent.atomic.AtomicLong;

import static com.dimer.algoritmos.ordenacao.SortUtils.swap;

public class InsertionSort {

    private AtomicLong comparatorCount = new AtomicLong();
    private long swapCount = 0;

    public InsertionSort(){}
    public <T> ExecutionLog sort(T[] array, Comparator<T> comparator) {
        comparator = getComparatorWithCount(comparator);

        for (int i = 1; i < array.length; i++) {

            int j = i;

            while (j > 0 && comparator.compare(array[j], array[j - 1]) < 0) {
                swap(array, j, j - 1);
                swapCount++;
                j -= 1;
            }
        }

        ExecutionLog executionLog = new ExecutionLog("Insertion sort", swapCount, comparatorCount.get());
        resetCounters();

        return executionLog;
    }

    private <T> Comparator<T> getComparatorWithCount(Comparator<T> comparator) {
        return (a, b) -> {
            comparatorCount.getAndIncrement();
            return comparator.compare(a, b);
        };
    }

    private void resetCounters() {
        swapCount = 0;
        comparatorCount.set(0);
    }
}
