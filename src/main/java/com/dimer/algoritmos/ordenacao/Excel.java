package com.dimer.algoritmos.ordenacao;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;

public class Excel {
    private Workbook workbook;
    private Sheet sheet;
    private int rowCount = 1;

    public Excel() {
        workbook = new XSSFWorkbook();
        sheet = workbook.createSheet("Algoritimos de ordenação");

        createHeader();
    }

    private void createHeader() {
        Row header = sheet.createRow(0);

        Cell cell = header.createCell(0);
        cell.setCellValue("ALGORITMOS");

        cell = header.createCell(1);
        cell.setCellValue("COMPARAÇÕES");

        cell = header.createCell(2);
        cell.setCellValue("TROCAS");

        cell = header.createCell(3);
        cell.setCellValue("EXECUÇÃO (ms)");
    }

    public void writeLogRow(ExecutionLog log) {
        Row row = sheet.createRow(rowCount++);

        Cell cell = row.createCell(0);
        cell.setCellValue(log.getAlgorithm());

        cell = row.createCell(1);
        cell.setCellValue(log.getComparations());

        cell = row.createCell(2);
        cell.setCellValue(log.getSwaps());

        cell = row.createCell(3);
        cell.setCellValue(log.getTime());
    }

    public void save() {
        try {
            File resources = new File("./src/main/resources/");
            String filePath = resources.getAbsolutePath() + "/execution.xlsx";

            FileOutputStream outputStream = new FileOutputStream(filePath);
            workbook.write(outputStream);
            workbook.close();
        } catch (Exception e) {
            System.out.println("Não foi possível salvar o arquivo.");
            e.printStackTrace();
        }
    }
}
