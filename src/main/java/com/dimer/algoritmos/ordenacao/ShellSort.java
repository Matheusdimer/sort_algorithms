package com.dimer.algoritmos.ordenacao;

import java.util.Comparator;
import java.util.concurrent.atomic.AtomicLong;

public class ShellSort {

    private final AtomicLong comparatorCount = new AtomicLong();
    private long swapCount = 0;

    public <T> ExecutionLog sort(T[] array, Comparator<T> comparator) {
        Comparator<T> comparatorWithCounter = getComparatorWithCount(comparator);

        shellSort(array, comparatorWithCounter);

        ExecutionLog executionLog = new ExecutionLog("Shell sort", swapCount, comparatorCount.get());
        resetCounters();

        return executionLog;
    }

    private <T> void shellSort(T[] array, Comparator<T> comparator) {
        int arrayLength = array.length;

        for (int interval = arrayLength / 2; interval > 0; interval /= 2) {
            for (int i = interval; i < arrayLength; i += 1) {
                T temp = array[i];
                int j;

                for (j = i; j >= interval && comparator.compare(array[j - interval], temp) > 0; j -= interval) {
                    array[j] = array[j - interval];
                }
                array[j] = temp;
                swapCount++;
            }
        }
    }


    private <T> Comparator<T> getComparatorWithCount(Comparator<T> comparator) {
        return (a, b) -> {
            comparatorCount.getAndIncrement();
            return comparator.compare(a, b);
        };
    }

    private void resetCounters() {
        swapCount = 0;
        comparatorCount.set(0);
    }
}
