package com.dimer.algoritmos.ordenacao;

import java.text.DecimalFormat;

public class ExecutionLog {
    private String algorithm;
    private long time;
    private long swaps;
    private long comparations;

    public ExecutionLog(String algorithm, long swaps, long comparations) {
        this.algorithm = algorithm;
        this.swaps = swaps;
        this.comparations = comparations;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getSwaps() {
        return swaps;
    }

    public void setSwaps(long swaps) {
        this.swaps = swaps;
    }

    public long getComparations() {
        return comparations;
    }

    public void setComparations(long comparations) {
        this.comparations = comparations;
    }

    public void print() {
        final DecimalFormat df = new DecimalFormat("###,###,###,###,###,###,###.##");

        System.out.printf(
                "\n========== %s ==========\nComparações: %s\nTrocas: %s\nTempo de execução: %d ms\n",
                algorithm, df.format(comparations), df.format(swaps), time
        );
    }
}
