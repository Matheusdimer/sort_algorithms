package com.dimer.algoritmos.ordenacao;

import java.util.Comparator;
import java.util.concurrent.atomic.AtomicLong;

public class MergeSort {

    private AtomicLong comparatorCount = new AtomicLong();
    private long swapCount = 0;

    private <T> void merge(T[] arr, int l, int m, int r, Comparator<T> comparator) {
        int n1 = m - l + 1;
        int n2 = r - m;

        T[] L = (T[]) new Object[n1];
        T[] R = (T[]) new Object[n2];

        for (int i = 0; i < n1; ++i) {
            L[i] = arr[l + i];
        }

        for (int j = 0; j < n2; ++j) {
            R[j] = arr[m + 1 + j];
        }

        int i = 0, j = 0;

        int k = l;
        while (i < n1 && j < n2) {
            if (comparator.compare(L[i], R[j]) <= 0) {
                swapCount++;
                arr[k] = L[i];
                i++;
            } else {
                swapCount++;
                arr[k] = R[j];
                j++;
            }
            k++;
        }

        while (i < n1) {
            arr[k] = L[i];
            i++;
            k++;
        }

        while (j < n2) {
            arr[k] = R[j];
            j++;
            k++;
        }
    }

    public <T> ExecutionLog sort(T[] arr, int l, int r, Comparator<T> comparator) {
        comparator = getComparatorWithCount(comparator);

        if (l < r) {
            int m = l + (r - l) / 2;

            sort(arr, l, m, comparator);
            sort(arr, m + 1, r, comparator);

            merge(arr, l, m, r, comparator);
        }

        ExecutionLog executionLog = new ExecutionLog("Merge sort", swapCount, comparatorCount.get());

        resetCounters();

        return executionLog;
    }

    private <T> Comparator<T> getComparatorWithCount(Comparator<T> comparator) {
        return (a, b) -> {
            comparatorCount.getAndIncrement();
            return comparator.compare(a, b);
        };
    }

    private void resetCounters() {
        swapCount = 0;
        comparatorCount.set(0);
    }
}
