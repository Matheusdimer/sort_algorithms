package com.dimer.algoritmos.ordenacao;

import java.util.Comparator;

public class HeapSort {
    public <T> void sort(T[] array, Comparator<T> comparator) {
        int n = array.length;

        for (int i = n / 2 - 1; i >= 0; i--) {
            heapify(array, n, i, comparator);
        }

        for (int i = n - 1; i > 0; i--) {
            SortUtils.swap(array, 0, i);

            heapify(array, i, 0, comparator);
        }
    }

    private <T> void heapify(T[] array, int n, int i, Comparator<T> comparator) {
        int largest = i;
        int l = 2 * i + 1;
        int r = 2 * i + 2;

        if (l < n && comparator.compare(array[l], array[largest]) > 0)
            largest = l;

        if (r < n && comparator.compare(array[r], array[largest]) > 0) {
            largest = r;
        }

        if (largest != i) {
            SortUtils.swap(array, i, largest);

            heapify(array, n, largest, comparator);
        }
    }
}
