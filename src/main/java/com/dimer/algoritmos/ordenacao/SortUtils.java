package com.dimer.algoritmos.ordenacao;

import java.text.DecimalFormat;
import java.util.Arrays;

public class SortUtils {
    private static Integer[] arrayDefault;
    private static long start;
    private static long end;

    public static <T> void swap(T[] array, int a, int b) {
        T temp = array[a];
        array[a] = array[b];
        array[b] = temp;
    }

    public static void printCounters(final String method, final long compareCount, final long swapCount) {
        final DecimalFormat df = new DecimalFormat("###,###,###,###,###,###,###.##");

        System.out.printf(
                "========== %s ==========\nComparações: %s\nTrocas: %s\n",
                method, df.format(compareCount), df.format(swapCount)
        );
    }

    public static void printCounters(final ExecutionLog log) {
        printCounters(log.getAlgorithm(), log.getComparations(), log.getSwaps());
    }

    public static void printArray(Object[] array) {
        System.out.println(Arrays.toString(array));
    }

    public static void start() {
        start = System.currentTimeMillis();
    }

    public static long end() {
        end = System.currentTimeMillis();

        return end - start;
    }

    public static void createIntegerArray(int numberOfElements) {
        if(numberOfElements > 0) {
            arrayDefault = generateRandomIntArray(numberOfElements);
        } else {
            throw new RuntimeException("Você não pode criar um vetor com quantidade negativa de números");
        }
    }

    public static Integer[] getIntegerArray() {
        if (arrayDefault != null && arrayDefault.length > 0) {
            return Arrays.copyOf(arrayDefault, arrayDefault.length);
        }
        throw new RuntimeException("O Array não foi inicilizado");
    }

    private static Integer[] generateRandomIntArray(int numberOfElemnts) {
        Integer[] array = new Integer[numberOfElemnts];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * numberOfElemnts);
        }
        return array;
    }
}
