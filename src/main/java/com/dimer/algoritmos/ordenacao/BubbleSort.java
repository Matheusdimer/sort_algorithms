package com.dimer.algoritmos.ordenacao;

import java.util.Comparator;

import static com.dimer.algoritmos.ordenacao.SortUtils.swap;

public class BubbleSort {

    private int i, j;
    private boolean hasChange;

    private long compareCount = 0;
    private long swapCount = 0;

    public <T> ExecutionLog sort(T[] array, Comparator<T> comparator) {
        ExecutionLog executionLog;

        for (i = 0; i < array.length; i++)
        {
            hasChange = false;
            for (j = array.length - 1; j > 0; j--)
            {
                compareCount++;
                if (comparator.compare(array[j], array[j - 1]) < 0) {
                    hasChange = true;
                    swap(array, j, j - 1);
                    swapCount++;
                }
            }
            if (!hasChange) {
                executionLog = new ExecutionLog("Bubble sort", swapCount, compareCount);
                resetCounters();
                return executionLog;
            }
        }

        executionLog = new ExecutionLog("Bubble sort", swapCount, compareCount);
        resetCounters();

        return executionLog;
    }

    private void resetCounters() {
        swapCount = 0;
        compareCount = 0;
    }
}
