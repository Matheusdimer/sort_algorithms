package com.dimer.algoritmos.ordenacao;

import java.util.Comparator;
import java.util.concurrent.atomic.AtomicLong;

import static com.dimer.algoritmos.ordenacao.SortUtils.printCounters;
import static com.dimer.algoritmos.ordenacao.SortUtils.swap;

public class QuickSort {

    private final AtomicLong comparatorCount = new AtomicLong();
    private long swapCount = 0;

    public <T> ExecutionLog sort(T[] array, Comparator<T> comparator) {
        Comparator<T> comparatorWithCount = getComparatorWithCount(comparator);

        quickSort(array, 0, array.length - 1, comparatorWithCount);

        ExecutionLog executionLog = new ExecutionLog("Quick sort", swapCount, comparatorCount.get());

        resetCounters();

        return executionLog;
    }

    private <T> void quickSort(T[] array, int first, int end, Comparator<T> comparator) {
        if (end > first) {

            int pivot = divider(array, first, end, comparator);

            quickSort(array, first, pivot - 1, comparator);

            quickSort(array, pivot + 1, end, comparator);

        }
    }

    private <T> int divider(T[] array, int firstIndex, int lastIndex, Comparator<T> comparator) {
        T pivot = array[firstIndex];
        int left = firstIndex + 1;
        int right = lastIndex;

        while (left <= right) {
            while (left <= right && comparator.compare(array[left], pivot) <= 0) {
                left++;
            }
            while (right >= left && comparator.compare(array[right], pivot) > 0) {
                right--;
            }
            if (left < right) {
                swap(array, right, left);
                swapCount++;
                left++;
                right--;
            }
        }
        swap(array, firstIndex, right);
        swapCount++;
        return right;
    }

    private <T> Comparator<T> getComparatorWithCount(Comparator<T> comparator) {
        return (a, b) -> {
            comparatorCount.getAndIncrement();
            return comparator.compare(a, b);
        };
    }

    private void resetCounters() {
        swapCount = 0;
        comparatorCount.set(0);
    }
}
