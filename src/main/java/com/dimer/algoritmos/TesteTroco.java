package com.dimer.algoritmos;

import com.dimer.algoritmos.outros.Troco;

public class TesteTroco {
    public static void main(String[] args) {
        final int[] moedasBrasil = { 100, 50, 25, 10, 5, 1 };
        final int valor = 66;

        System.out.println("Moedas para o troco de " + valor + " centavos.");
        System.out.println(Troco.guloso(moedasBrasil, valor));
    }
}
