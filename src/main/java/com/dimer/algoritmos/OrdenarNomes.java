package com.dimer.algoritmos;

import com.dimer.algoritmos.ordenacao.QuickSort;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class OrdenarNomes {
    public static void main(String[] args) throws IOException {
        File file = new File("./src/main/resources/nomes.txt");
        InputStream inputStream = new FileInputStream(file);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        final List<String> nomes = new ArrayList<>();
        final QuickSort quickSort = new QuickSort();

        String row;

        while ((row = bufferedReader.readLine()) != null) {
            nomes.add(row);
        }

        final String[] array = new String[nomes.size()];

        quickSort.sort(nomes.toArray(array), String::compareTo);

        File orderedFile = new File("./src/main/resources/nomes_ordenados.txt");
        OutputStream outputStream = new FileOutputStream(orderedFile);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        BufferedWriter writer = new BufferedWriter(outputStreamWriter);

        for (String nome : array) {
            writer.write(nome);
            writer.newLine();
        }

        writer.flush();
        writer.close();
    }
}
