package com.dimer.algoritmos;

import com.dimer.algoritmos.outros.Fatorial;
import com.dimer.algoritmos.outros.Fibonacci;

public class Calculos {
    private static final int N_FIBONACCI = 15;
    private static final int N_FATORIAL = 5;

    public static void main(String[] args) {
        System.out.printf(
                "Fatorial de %d:\nRecursivo: %d\nIterativo: %d\n%n",
                N_FATORIAL,
                Fatorial.recursivo(N_FATORIAL),
                Fatorial.iterativo(N_FATORIAL)
        );

        System.out.printf(
                "Número %d da sequência de Fibonacci:\nIterativo: %d\nRecursivo: %d\n",
                N_FIBONACCI,
                Fibonacci.iterativo(N_FIBONACCI),
                Fibonacci.recursivo(N_FIBONACCI)
        );
    }
}
