package com.dimer.algoritmos;

import com.dimer.algoritmos.ordenacao.HeapSort;
import com.dimer.algoritmos.outros.BinaryInsertionSort;
import com.dimer.algoritmos.outros.CombSort;
import com.dimer.algoritmos.outros.SelectionSort;
import com.dimer.algoritmos.outros.Troco;

import java.util.Arrays;
import java.util.Comparator;

import static com.dimer.algoritmos.ordenacao.SortUtils.*;

public class TesteOrdenacao {

    public static void main(String[] args) {
        SelectionSort selectionSort = new SelectionSort();
        CombSort combSort = new CombSort();

        createIntegerArray(100);
        Integer[] array = getIntegerArray();

        System.out.println("Executando algoritmos de ordenação para o array:");
        System.out.println(Arrays.toString(array));

        // Comb Sort

        combSort.sort(array, Comparator.comparingInt(a -> a));

        System.out.println("Ordenação Comb Sort:");
        System.out.println(Arrays.toString(array));


        // Heap Sort
        array = getIntegerArray();

        HeapSort heapSort = new HeapSort();

        heapSort.sort(array, Comparator.comparingInt(a -> a));

        System.out.println("Ordenação Heap Sort:");
        System.out.println(Arrays.toString(array));

        // Binary insertion sort
        array = getIntegerArray();

        BinaryInsertionSort binaryInsertionSort = new BinaryInsertionSort();

        binaryInsertionSort.sort(array, array.length);

        System.out.println("Ordenação Binary Insertion sort:");
        System.out.println(Arrays.toString(array));
    }
}
