package com.dimer.algoritmos;

import com.dimer.algoritmos.outros.Pesquisa;

import java.util.Arrays;

public class Pesquisas {
    private static final int PESQUISA = 62;

    public static void main(String[] args) {
        final int[] array = {4, 15, 24, 13, 8, 92, 74, 62, 35, 17};

        Arrays.sort(array);

        System.out.printf("Array ordenado: %s", Arrays.toString(array));
        System.out.printf(
                "\nPesquisa pelo elemento %d:\nSequencial: índice %d\nBinária: índice %d\n",
                PESQUISA,
                Pesquisa.sequencial(array, PESQUISA),
                Pesquisa.binaria(array, PESQUISA)
        );

    }
}
